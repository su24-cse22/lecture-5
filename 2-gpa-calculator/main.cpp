#include <iostream>
#include <iomanip>
#include "gpa.h"
using namespace std;

int main() {

    /*

        Create a GPA calculator app based on the 
        folling criteria:
    
        A+ = 4.3 grade points
        A  = 4 grade points
        A- = 3.7 grade points
        B+ = 3.3 grade points
        B  = 3 grade points
        B- = 2.7 grade points
        C+ = 2.3 grade points
        C  = 2 grade points
        C- = 1.7 grade points
        D+ = 1.3 grade points
        D  = 1 grade point
        D- = 0.7 grade points
        F  = 0 grade points

        To calculate GPA, you must first compute
        the grade points for each course by multiplying
        the course credits by letter grade points. Then,
        you add up the grade points for all courses and
        divide by total number of credits.

        Example:
        ====================
        GPA CALCULATOR
        ====================
        Input math grade: A
        Input math grade credits: 4

        Input science grade: B+
        Input science grade credits: 4

        Input english grade: C
        Input english grade credits: 3

        Input history grade: D-
        Input history grade credits: 4

        GPA: 2.53

    */

    printHeading();

    string mathGrade, scienceGrade, englishGrade, historyGrade;
    int mathCredits, scienceCredits, englishCredits, historyCredits;

    cout << "Input math grade: ";
    cin >> mathGrade;
    cout << "Input math grade credits: ";
    cin >> mathCredits;
    cout << endl;

    cout << "Input science grade: ";
    cin >> scienceGrade;
    cout << "Input science grade credits: ";
    cin >> scienceCredits;
    cout << endl;

    cout << "Input english grade: ";
    cin >> englishGrade;
    cout << "Input english grade credits: ";
    cin >> englishCredits;
    cout << endl;

    cout << "Input history grade: ";
    cin >> historyGrade;
    cout << "Input history grade credits: ";
    cin >> historyCredits;
    cout << endl;

    float mathGradePoints = calculateGradePoints(mathGrade, mathCredits);
    float scienceGradePoints = calculateGradePoints(scienceGrade, scienceCredits);
    float englishGradePoints = calculateGradePoints(englishGrade, englishCredits);
    float historyGradePoints = calculateGradePoints(historyGrade, historyCredits);

    float GPA = calculateGPA(mathCredits, scienceCredits, englishCredits, historyCredits, mathGradePoints, scienceGradePoints, englishGradePoints, historyGradePoints);

    cout << fixed << setprecision(2);
    cout << "GPA: " << GPA << endl;

    return 0;
}