#ifndef GPA_H
#define GPA_H

#include <iostream>

void printHeading() {
    std::cout << "====================" << std::endl;
    std::cout << "   GPA CALCULATOR" << std::endl;
    std::cout << "====================" << std::endl;
}

float getGradePoints(std::string grade) {
    if (grade == "A+") {
        return 4.3;
    }
    else if (grade == "A") {
        return 4;
    }
    else if (grade == "A-") {
        return 3.7;
    }
    else if (grade == "B+") {
        return 3.3;
    }
    else if (grade == "B") {
        return 3;
    }
    else if (grade == "B-") {
        return 2.7;
    }
    else if (grade == "C+") {
        return 2.3;
    }
    else if (grade == "C") {
        return 2;
    }
    else if (grade == "C-") {
        return 1.7;
    }
    else if (grade == "D+") {
        return 1.3;
    }
    else if (grade == "D") {
        return 1;
    }
    else if (grade == "D-") {
        return 0.7;
    }
    else {
        return 0;
    }
}

float calculateGradePoints(std::string grade, int credits) {
    float gradePoints = getGradePoints(grade);
    return gradePoints * credits;
}

float calculateGPA(int mathCredits, int scienceCredits, int englishCredits, int historyCredits, float mathGradePoints, float scienceGradePoints, float englishGradePoints, float historyGradePoints) {
    float totalCredits = mathCredits + scienceCredits + englishCredits + historyCredits;
    float totalGradePoints = mathGradePoints + scienceGradePoints + englishGradePoints + historyGradePoints;
    return totalGradePoints / totalCredits;
}

#endif
