#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <iostream>

void printHeading() {
    std::cout << "====================" << std::endl;
    std::cout << "    CALCULATOR" << std::endl;
    std::cout << "====================" << std::endl;
}

void printMenu() {
    system("clear");

    printHeading();
    std::cout << "1. Addition" << std::endl;
    std::cout << "2. Subtraction" << std::endl;
    std::cout << "3. Multiplication" << std::endl;
    std::cout << "4. Division" << std::endl;
}

float handleAddition(float x, float y) {
    return x + y;
}

float handleSubtraction(float x , float y) {
    return x - y;
}

float handleMultiplication(float x, float y) {
    return x * y;
}

float handleDivision(float x, float y) {
    return x / y;
}

void handleMathOperation(int choice) {
    system("clear");

    printHeading();

    if (choice == 1) {
        std::cout << "Addition" << std::endl;
    }
    else if (choice == 2) {
        std::cout << "Subtraction" << std::endl;
    }
    else if (choice == 3) {
        std::cout << "Multiplication" << std::endl;
    }
    else if (choice == 4) {
        std::cout << "Division" << std::endl;
    }

    std::cout << "--------------------" << std::endl;

    float x, y;

    std::cout << "Enter x: ";
    std::cin >> x;

    std::cout << "Enter y: ";
    std::cin >> y;

    if (choice == 1) {
        std::cout << x << " + " << y << " = " << handleAddition(x, y) << std::endl;
    }
    else if (choice == 2) {
        std::cout << x << " - " << y << " = " << handleSubtraction(x, y) << std::endl;
    }
    else if (choice == 3) {
        std::cout << x << " * " << y << " = " << handleMultiplication(x, y) << std::endl;
    }
    else if (choice == 4) {
        std::cout << x << " / " << y << " = " << handleDivision(x, y) << std::endl;
    }
}

#endif