#include <iostream>
#include "calculator.h"
using namespace std;

int main() {

    /*
    
        Create a calculator app that supports
        addition, subtraction, multiplication,
        and division.

        Example:
        ====================
            CALCULATOR
        ====================
        1. Addition
        2. Subtraction
        3. Multiplication
        4. Division
        Enter your choice: 3


        ====================
            CALCULATOR
        ====================
        Multiplication
        --------------------
        Enter x: 7
        Enter y: 42
        7 * 42 = 294

    */

    printMenu();

    int choice;
    cout << "Enter your choice: ";
    cin >> choice;

    handleMathOperation(choice);

    return 0;
}