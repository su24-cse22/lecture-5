#include <iostream>
using namespace std;

void printHelloWorld() {
    cout << "Hello World!" << endl;
}

void printGreeting(string name) {
    cout << "Hello, " << name << endl;
}

string getLetterGrade(int grade) {
    if (grade >= 90) {
        return "A";
    }
    else if (grade >= 80) {
        return "B";
    }
    else if (grade >= 70) {
        return "C";
    }
    else if (grade >= 60) {
        return "D";
    }
    else {
        return "F";
    }
}

float calculateTax(float price, float taxPercentage) {
    return price * (taxPercentage / 100);
}

bool isEven(int x) {
    return x % 2 == 0;
}

int main() {

    /*
    
        Create a void function named `printHelloWorld` that
        prints the message "Hello World!".

        Create a void function named `printGreeting` that
        takes in the name of a person and prints the message
        "Hello, {name}".

        Create a function named `getLetterGrade` that takes 
        in a number, representing a grade, and returns the 
        appropriate letter grade.

        Create a function named `calculateTax` that takes
        the price and sales tax percentage and returns the
        sales tax amount.

        Create a function named `isEven` that takes in a
        number and determines if it is even. The function
        should return a boolean.

    */

    // printHelloWorld();

    // printGreeting("Rufus Bobcat");

    // string letterGrade = getLetterGrade(3);
    // cout << letterGrade << endl;

    // float taxAmount = calculateTax(45000, 7.25);
    // cout << taxAmount << endl;

    cout << isEven(42) << endl;

    return 0;
}